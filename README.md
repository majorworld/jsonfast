# JsonMini 当前版本1.0.12

[查看更新日志表格](https://gitee.com/majorworld/jsonmini#5%E6%9B%B4%E6%96%B0%E6%97%A5%E5%BF%97)

#### （1）介绍
JSON序列化工具，无第三方依赖，可单独复制[JsonMini.cs](https://gitee.com/majorworld/jsonmini/raw/master/JsonMini/JsonMini.cs)类文件使用，仅1000多行代码

#### （2）软件架构
最低适用于net 4.0及以后版本,除了数组、集合和基础类型，还包括Color、Point、DataTable、DataSet和Byte[]的序列化和反序列化，兼容Newstonsoft.Json序列化结果

#### （3）安装教程

复制[JsonMini.cs](https://gitee.com/majorworld/jsonmini/raw/master/JsonMini/JsonMini.cs)一个文件到自己项目中，就可以使用两个拓展方法JSONFrom()反序列化和JSONTo()序列化

#### （4）使用说明


##### 1、反序列化为字典，返回这种类型比Newstonsoft.Json更快

```cs
string str = "{\"state\":true,\"time\":\"2020-10-10 1:2:1\",\"num\":-33,\r\n\t\f     \"name\":\"你好\r\n\t\f左\b右，\\\"世界\\\"\",\"age\":9.9,\"yy\":{\"sex\":null}}";
Dictionary<string, object> t1 = str.JSONFrom();

```

##### 2、反序列化为实体类或动态类
```cs
string str = "{\"Name\":\"高老师\",\"Num\":3.1415926,\"Col\":\"2,3,4\"}";
Teacher t1 = str.JSONFrom<Teacher>();
dynamic t2 = str.JSONFrom<dynamic>();
```


##### 3、对象或集合序列化字符串
```cs
List<Student> list = new List<Student>();
string t1 = list.JSONTo();
```

##### 4、判断是不是数组
```cs
string str1 = "{\"A\":{\"a\":1,\"b\":2}}";
string str2 = "{\"A\":[1,2]}";

var dy1 = str1.JSONFrom();
if (dy1["A"] is IList)
    Console.WriteLine("d1里是数组");

var dy2 = str2.JSONFrom();
if (dy2["A"] is IList)
    Console.WriteLine("d2里是数组");
```


##### 5、类似Newstonsoft的JObject和JArray
```cs
//JObject
Dictionary<string, object> dict = new Dictionary<string, object>();
dict.Add("name", "tom");
dict.Add("age", 18);

//JArray
List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
list.Add(dict);

//序列化和反序列化
var stringArr = list.JsonTo();
Console.WriteLine(stringArr);
var Arr = stringArr.JsonFrom<List<Dictionary<string, object>>>();
```

##### 6、如果有字段存在无限递归引用，抛出异常System.StackOverflowException:“Exception of type 'System.StackOverflowException' was thrown.”，可使用[FastIgnore]特性过滤掉问题字段
```cs
class UserItem
{
    public User User { get; set; }
    public int UserId { get; set; }
}
class User
{
    public string Name { get; set; }
    //过滤掉无限递归引用的问题字段
    [FastIgnore] 
    public UserItem Bad { get; set; }
}
```

##### 7、支持对unicode字符串的解析
```cs
string str = "{\"Name\":\"\\u4f60\\u597d\"}";
string json = str.JsonFrom().JsonTo();
Console.WriteLine(json);// "{\"Name\":\"你好\"}";
```

##### 8、添加动态获取字段
```cs
//第二个参数，传入空格分隔的字符串，直接获取到tgt字段的内容
string result = "{\"type\":\"ZH_CN2EN\",\"errorCode\":0,\"elapsedTime\":0,\"translateResult\":[[{\"src\":\"有人在家吗\",\"tgt\":\"Is anyone home?\"}]]}";
var tgt= JsonMini.PickData(result, "translateResult 0 0 tgt");

//获取到数组类型元素的数量，然后按该数量循环，使用PickData依次获取数组里所有元素
string json = "{\"Student\":[{\"Name\":\"Lucy\"},{\"Name\":\"Jack\"},{\"Name\":\"Tom\"}]}";
int count = JsonMini.ArrayCount(json, "Student");
```

##### 9、支持对转义字符的解析
```cs

        public class TT
        {
            public string[] Data { get; set; }
        }
        static void Main(string[] args)
        {

            string s = "{\"Data\":[\"a\",\"回车符\\r换行符\\n退格符\\b水平制表符\\t换页符\\f单引号\\\'双引号\\\"\"]}";

            var x = Newtonsoft.Json.JsonConvert.DeserializeObject<TT>(s);
            var y = s.JsonFrom<TT>();
            
            Console.WriteLine("--------------------------------");
            Console.WriteLine(x.Data[1]);
            Console.WriteLine(y.Data[1]);
            Console.WriteLine("--------------------------------");
            var xx = Newtonsoft.Json.JsonConvert.SerializeObject(x);
            Console.WriteLine(xx);
            var yy = y.JsonTo();
            Console.WriteLine(yy);
        }
```

![输入图片说明](https://gitee.com/majorworld/JsonMini/raw/master/%E6%94%AF%E6%8C%81%E8%BD%AC%E4%B9%89%E5%AD%97%E7%AC%A6%E7%9A%84%E6%B5%8B%E8%AF%95.png)


#### （5）更新日志


| 更新日志 |版本|
|------------|--|
| 2023-11-16 |1.0.12|
| 1、进行优化
| 2023-09-19 |1.0.11|
| 1、按群友的反馈，添加了对字符串二次转义后的解析支持
| 2023-09-16 |1.0.10|
| 1、添加了在实体类继承父类时，同样按父类的FastIgnore特性去过滤
| 2、按照用户MLing592建议增加了类似JsonConvert.DeserializeObject(string value, Type t)的序列化
| 2023-09-06 |1.0.9|
| 1、修复了序列化时没有将字符串里转义字符进行处理
| 2023-05-31 |1.0.8|
| 1、修复了序列化时没有将对象里的双引号进行转义，导致反序列化失败的严重bug||
| 2023-05-15 |1.0.7|
| 1、添加动态获取字段，PickData和ArrayCount方法||
| 2023-01-03 |1.0.6|
| 1、优化代码结构，防止私有拓展方法全局污染||
| 2022-12-29 |1.0.5|
| 1、添加对unicode字符串的解析||
| 2022-12-28 |1.0.4|
| 1、修复了转DataTable时，字段存在null报错问题 ||
| 2、添加了对nullable的支持 ||
| 3、修改命名空间为System，方便使用 ||
| 2022-12-26 |1.0.3|
| 1、添加了对枚举的反序列化支持 ||


 #### （6）测试

![速度测试](https://gitee.com/majorworld/jsonmini/raw/master/%E9%80%9F%E5%BA%A6%E6%B5%8B%E8%AF%95.png)

```cs

    //10万次序列化测试，JsonMini耗时559毫秒，Newtonsoft.Json耗时1382毫秒
    Test.TimeTest();//测试耗时
    Test.JsonTest();//测试转义字符

    public class Test
    {
        public class TT
        {
            public string[] Data { get; set; }
        }
        public static void JsonTest()
        {
            string s = "{\"Data\":[\"a\",\"空字符\0故障码\a回车符\r换行符\n退格符\b水平制表符\t垂直制表符\v换页符\f单引号\'双引号\\\"\"]}";

            var x = Newtonsoft.Json.JsonConvert.DeserializeObject<TT>(s);
            var y = s.JsonFrom<TT>();

            Console.WriteLine("--------------------------------");
            Console.WriteLine(x.Data[1]);
            Console.WriteLine(y.Data[1]);
            Console.WriteLine("--------------------------------");
            var x2 = Newtonsoft.Json.JsonConvert.SerializeObject(x);
            Console.WriteLine(x2);
            var y2 = y.JsonTo();
            Console.WriteLine(y2);

            var x3 = Newtonsoft.Json.JsonConvert.DeserializeObject<TT>(x2);
            var y3 = y2.JsonFrom<TT>();

            var x4 = Newtonsoft.Json.JsonConvert.SerializeObject(x3);
            Console.WriteLine(x4);
            var y4 = y3.JsonTo();
            Console.WriteLine(y4);
        }
        public class Tearcher
        {
            public string Name { get; set; }
            public float Num { get; set; }
            public Color Col { get; set; }
        }

        public static void TimeTest(int count = 100000)
        {
            string s = "{\"Name\":\"高老师\",\"Num\":3.1415926,\"Col\":\"2,3,4\"}";
            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                var a1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Tearcher>(s);
                var a2 = Newtonsoft.Json.JsonConvert.SerializeObject(a1);
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                var a1 = s.JsonFrom<Tearcher>();
                var a2 = a1.JsonTo();
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
        }
    }

```


