﻿using BenchmarkDotNet.Running;
using System;
using TouchSocket.Core.IO;

namespace JsonFastBenchmark
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ConsoleAction consoleAction = new ConsoleAction();
            consoleAction.Add("1", "简单对象测试", () => { BenchmarkRunner.Run<BenchmarkSimple>(); });
            consoleAction.Add("2", "复杂对象测试", () => { BenchmarkRunner.Run<BenchmarkComplex>(); });
            consoleAction.ShowAll();
            while (true)
            {
                consoleAction.Run(Console.ReadLine());
            }
        }
    }
}
