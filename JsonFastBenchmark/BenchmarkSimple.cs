﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using Microsoft.Diagnostics.Tracing.Analysis.GC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonFastBenchmark
{
    [SimpleJob(RuntimeMoniker.Net461)]
    [SimpleJob(RuntimeMoniker.Net60)]
    [SimpleJob(RuntimeMoniker.Net70)]
    [MemoryDiagnoser]
    public class BenchmarkSimple
    {
        public const int Count = 10000;

        [Benchmark(Baseline = true)]
        public void DirectNew_SimpleObject()
        {
            var v = new SimpleObject { Age = 40, Name = "John" };
            for (int i = 0; i < Count; i++)
            {
                byte[] bytes = new byte[8];//开辟小内存模拟序列化
                var val = new SimpleObject { Age = 40, Name = "John" };//直接新建，模拟反序列化
            }
        }

        [Benchmark]
        public void NewtonsoftJson_SimpleObject()
        {
            var v = new SimpleObject { Age = 40, Name = "John" };
            for (int i = 0; i < Count; i++)
            {
                var str = Newtonsoft.Json.JsonConvert.SerializeObject(v);
                var val = Newtonsoft.Json.JsonConvert.DeserializeObject<SimpleObject>(str);
            }
        }

        [Benchmark]
        public void JsonFast_SimpleObject()
        {
            var v = new SimpleObject { Age = 40, Name = "John" };
            for (int i = 0; i < Count; i++)
            {
                var str = JsonFast.JsonFastConverter.JsonTo(v);
                var val = JsonFast.JsonFastConverter.JsonFrom<SimpleObject>(str);
            }
        }

        [Benchmark]
        public void TouchSocketJson_SimpleObject()
        {
            var v = new SimpleObject { Age = 40, Name = "John" };
            for (int i = 0; i < Count; i++)
            {
                var str = TouchSocket.Core.Serialization.JsonWriter.ToJson(v);
                var val = TouchSocket.Core.Serialization.JsonParser.FromJson<SimpleObject>(str);
            }
        }
    }

    public class SimpleObject 
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }


    
}
