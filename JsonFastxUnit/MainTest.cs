using JsonFastBenchmark;

namespace JsonFastxUnit
{
    public class MainTest
    {
        [Fact]
        public void SimpleObjectShouldBeOk()
        {
            var v = new SimpleObject { Age = 40, Name = "John" };

            var str = JsonFast.JsonFastConverter.JsonTo(v);
            var val = JsonFast.JsonFastConverter.JsonFrom<SimpleObject>(str);

            Assert.NotNull(val);
            Assert.Equal(v.Age,val.Age);
            Assert.Equal(v.Name,val.Name);
        }

        [Fact]
        public void SimpleObjectToNewtonsoftJsonShouldBeOk()
        {
            var v = new SimpleObject { Age = 40, Name = "John" };

            var str = JsonFast.JsonFastConverter.JsonTo(v);
            var val = Newtonsoft.Json.JsonConvert.DeserializeObject<SimpleObject>(str);

            Assert.NotNull(val);
            Assert.Equal(v.Age, val.Age);
            Assert.Equal(v.Name, val.Name);
        }
    }
}